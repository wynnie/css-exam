# css-exam

### Exercise Steps:
1. Fork the repository to your own gitlab account.
2. Clone the forked repository of your gitlab account to your local.
3. Using git as the version control tool.
4. Update the css file based on the page view design.
5. Import and use the css file in the html file.
6. Push the commits to your gitlab repository.

### Exercise Output:
1. Create a pull request from your repository.
2. Submit your pull request link.
